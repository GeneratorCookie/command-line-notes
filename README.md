# Introduction

This repository is a personal aggregations of command line tools that I use to develop and maintain my servers.

# Tools

## Internet connection & speed

Testing connection speed: `speedtest-cli`

## Mapping the network

Map local network: `sudo nmap -sn <ip-address>/24` (don't forget to set the last digit of the IP to 0)

Scan an IP address to find open ports: `nmap -vv <ip-address>`.

Scan an IP address with a port range: `nmap -vv -p <low-port>-<high-port> <ip-address>`

The `-vv` argument is to get a verbose output.

# Package Manager

## HomeBrew for Mac

Install new package: `brew install <package>`

Upgrade installed package: `brew upgrade <package>`

Remove installed package: `brew uninstall <package>`

List installed packages: `brew list`

# Linux OS Management

## Users

List all users: `cat /etc/passwd`

Create new SUDO user:
1. Create new user: `sudo useradd <username>`
1. Create new password `sudo passwd <username>`
1. Add user to the SUDO group: `sudo adduser <username> sudo`

Delete user:
1. Delete user with `/home` directory: `sudo userdel -r <username>`

## Host

View hostname: `hostname`

Change hostname:
1. Update the name here: `/etc/hostname`
1. Change all references to previous name here: `/etc/hosts`

Make sure to install `ntp` to always keep the time in sync

## OS

List information about the OS: `cat /etc/os-release`


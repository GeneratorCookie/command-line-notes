# How the Pis are setup

## Basic setup

1. Create a new SUDO user
1. Delete the Pi user
1. Make sure my package repository is up to date: `sudo apt-get update`
1. Install NTP for the clock to be in sync: `sudo apt-get install ntp`
1. Make sure the Pi with my dependencies are up to date: `sudo apt-get upgrade`
1. Install Zsh, instructions [here](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH)
1. Install OhMyZsh, instructions [here](https://github.com/ohmyzsh/ohmyzsh)
1. Intsall Tmux to create terminal sessions: `sudo apt-get install tmux`
1. Intsall nmon to better visualize resource utilization: `sudo apt-get install nom`
3. Remove `Apache2` as it's a security risk to have that port `80` open by default:
 1. `sudo apt-get --purge remove apache2`
 1. `sudo apt-get autoremove`
4. Change port of SSH to something random to make some automated attacks harder
5. Upload my SSH certificate to the Pi
6. Set the Dracula console theme, instructions [here](https://draculatheme.com/zsh/)
